$(document).ready(function(){
            
            $('#loginBtn').click(function(){
                $('#loginModal').modal("show");
            });
            $('#loginClose').click(function(){
                $('#loginModal').modal("hide");
            });
            $('#loginCancel').click(function(){
                $('#loginModal').modal("hide");
            });
            $('#signupBtn').click(function(){
                $('#signupModal').modal("show");
            });
            $('#signupClose').click(function(){
                $('#signupModal').modal("hide");
            });
            $('#signupCancel').click(function(){
                $('#signupModal').modal("hide");
            });
        });
    